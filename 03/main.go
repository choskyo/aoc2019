package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
}

func main() {
	file, _ := os.Open("./input")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	scanner.Scan()
	path1 := scanner.Text()

	scanner.Scan()
	path2 := scanner.Text()

	p1 := mapPath(path1)
	p2 := mapPath(path2)

	intersections := findIntersections(p1, p2)

	answer1 := getNearestIntersectionDistance(p1, p2, intersections)
	fmt.Println(answer1)

	answer2 := getNearestIntersectionSteps(p1, p2, intersections)
	fmt.Println(answer2)
}

func mapPath(path string) map[point]int {
	p := map[point]int{}
	stepCount := 1
	x, y := 0, 0

	for _, sect := range strings.Split(path, ",") {
		dir := string(sect[0])
		steps, _ := strconv.Atoi(sect[1:])

		for i := 0; i < steps; i++ {
			switch dir {
			case "U":
				y++
			case "D":
				y--
			case "L":
				x--
			case "R":
				x++
			}
			xy := point{x, y}
			p[xy] = stepCount
			stepCount++
		}
	}

	return p
}

func findIntersections(path1, path2 map[point]int) []point {
	intersections := []point{}

	for p := range path1 {
		if path2[p] != 0 {
			intersections = append(intersections, p)
		}
	}

	return intersections
}

func getNearestIntersectionDistance(p1, p2 map[point]int, intersections []point) int {
	minDistance := math.MaxInt64

	for _, p := range intersections {
		d := getDistance(p)

		if d < minDistance {
			minDistance = d
		}
	}

	return minDistance
}

func getNearestIntersectionSteps(p1, p2 map[point]int, intersections []point) int {
	minSteps := math.MaxInt64

	for _, p := range intersections {
		sumSteps := p1[p] + p2[p]

		if sumSteps < minSteps {
			minSteps = sumSteps
		}
	}

	return minSteps
}

func getDistance(p point) int {
	return int(math.Abs(float64(0-p.x)) + math.Abs(float64(0-p.y)))
}
