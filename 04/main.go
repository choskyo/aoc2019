package main

import (
	"fmt"
	"strconv"
)

const min = 353096
const max = 843212

func main() {
	passwords := []int{}

	for i := min; i < max; i++ {
		if !notDecreasing(i) || !hasDubs(i) {
			continue
		}

		passwords = append(passwords, i)
	}

	fmt.Println(len(passwords))

	passwords = []int{}
	for i := min; i < max; i++ {
		if !notDecreasing(i) || !hasOnlyDubs(i) {
			continue
		}

		passwords = append(passwords, i)
	}

	fmt.Println(len(passwords))
}

func notDecreasing(n int) bool {
	s := strconv.Itoa(n)

	for c := 1; c < len(s); c++ {
		cnum, _ := strconv.Atoi(string(s[c]))
		prevNum, _ := strconv.Atoi(string(s[c-1]))

		if cnum < prevNum {
			return false
		}
	}

	return true
}

// part 1
func hasDubs(n int) bool {
	s := strconv.Itoa(n)

	for c := 1; c < len(s); c++ {
		cnum, _ := strconv.Atoi(string(s[c]))
		prevNum, _ := strconv.Atoi(string(s[c-1]))

		if cnum == prevNum {
			return true
		}
	}

	return false
}

// part 2
func hasOnlyDubs(n int) bool {
	s := strconv.Itoa(n)
	current, _ := strconv.Atoi(string(s[0]))
	count := 1

	for c := 1; c < len(s); c++ {
		cnum, _ := strconv.Atoi(string(s[c]))

		if cnum == current {
			count++

			if c == len(s)-1 && count == 2 {
				return true
			}
			continue
		}

		if count == 2 {
			return true
		}

		current = cnum
		count = 1
	}

	return false
}
