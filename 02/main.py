def process(input):
    for i in range(0, len(input), 4):
        ins = int(input[i])

        if ins == OP_HLT:
            break

        add_1 = int(input[i+1])
        add_2 = int(input[i+2])
        out = int(input[i+3])

        if ins == OP_ADD:
            input[out] = int(input[add_1]) + int(input[add_2])
        elif ins == OP_MLT:
            input[out] = int(input[add_1]) * int(input[add_2])

    return input[0]


def part1(input):
    ip = input.copy()
    ip[1] = 12
    ip[2] = 2
    print(process(ip))


def part2(ip):
    for noun in range(0, 100, 1):
        for verb in range(0, 100, 1):
            vals = ip.copy()
            vals[1] = noun
            vals[2] = verb

            if process(vals) == 19690720:
                print(f"{noun}, {verb}")
                return


OP_HLT = 99
OP_ADD = 1
OP_MLT = 2

data = open("./input").readline().split(',')

part1(data)
part2(data)
