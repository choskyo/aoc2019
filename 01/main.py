def part1(lines):
    fuelSum = 0
    for line in lines:
        mass = float(line)

        fuel = (mass // 3) - 2

        fuelSum += fuel

    print(fuelSum)

    return fuelSum


def part2(lines):
    fuelSum = 0
    for line in lines:
        mass = float(line)
        fuel = (mass // 3) - 2

        while fuel >= 0:
            fuelSum += fuel
            fuel = (fuel // 3) - 2

    print(fuelSum)

    return fuelSum


data = open("./data_bigboy", "r").readlines()

part1(data)
part2(data)
