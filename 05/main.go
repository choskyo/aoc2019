package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

const testNum = 123456789

func main() {
	file, _ := os.Open("./input")
	scanner := bufio.NewScanner(file)
	scanner.Scan()
	input := strings.Split(scanner.Text(), ",")
	memory := []int{}

	for _, s := range input {
		v, _ := strconv.Atoi(s)
		memory = append(memory, v)
	}

	process(memory)
}
