package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var cursor = 0

func process(memory []int) int {
	iteration := 0
	for {
		iteration++
		instruction := memory[cursor]

		opcode := instruction % 100

		if opcode == opHalt {
			break
		}

		mode1 := getDigit(instruction, 2)
		mode2 := getDigit(instruction, 3)
		// mode3 := getDigit(instruction, 4)

		address1 := memory[cursor+1]
		address2 := memory[cursor+2]
		address3 := memory[cursor+3]

		switch opcode {
		case opAdd:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				memory[address3] = v1 + v2

				cursor += 4
			}
		case opMult:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				memory[address3] = v1 * v2

				cursor += 4
			}
		case opInput:
			{
				reader := bufio.NewReader(os.Stdin)
				fmt.Println("Awaiting input")
				input, _ := reader.ReadString('\n')
				input = strings.Replace(input, "\n", "", 1)
				iv, _ := strconv.Atoi(input)

				memory[address1] = iv

				cursor += 2
			}
		case opOut:
			{
				v1 := getVal(memory, address1, mode1)

				fmt.Printf("Output is: %v\n", v1)
				cursor += 2
			}
		case opJumpIfTrue:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				if v1 != 0 {
					cursor = v2
				} else {
					cursor += 3
				}
			}
		case opJumpIfFalse:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				if v1 == 0 {
					cursor = v2
				} else {
					cursor += 3
				}
			}
		case opLess:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				if v1 < v2 {
					memory[address3] = 1
				} else {
					memory[address3] = 0
				}

				cursor += 4
			}
		case opEquals:
			{
				v1 := getVal(memory, address1, mode1)
				v2 := getVal(memory, address2, mode2)

				if v1 == v2 {
					memory[address3] = 1
				} else {
					memory[address3] = 0
				}

				cursor += 4
			}
		default:
			{
				fmt.Printf("iteration: %v\n", iteration)
				fmt.Printf("cursor at: %v\n", cursor)
				fmt.Println(instruction)
				panic("Unknown opcode")
			}
		}
	}

	return memory[0]
}

// 0 = position mode, 1 = immediate mode
func getVal(memory []int, addr, mode int) int {
	if mode == 0 {
		return memory[addr]
	}

	return addr
}
