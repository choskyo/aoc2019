package main

const (
	opHalt        int = 99
	opAdd         int = 1
	opMult        int = 2
	opInput       int = 3
	opOut         int = 4
	opJumpIfTrue  int = 5
	opJumpIfFalse int = 6
	opLess        int = 7
	opEquals      int = 8
)

const (
	modePosition  int = 0
	modeImmediate int = 1
)
