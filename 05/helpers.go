package main

import "math"

// getDigit returns num[i] using 0-based Right to Left indexing
func getDigit(num, i int) int {
	return int(math.Floor(float64(float64(num)/math.Pow(10, float64(i))))) % 10
}
